class Card

  attr_reader :value, :suit

  VALUE_MAP = {
    1 => "A",
    2 => "2",
    3 => "3",
    4 => "4",
    5 => "5",
    6 => "6",
    7 => "7",
    8 => "8",
    9 => "9",
    10 => "10",
    11 => "J",
    12 => "Q",
    13 => "K"
  }

# #may want to add in unicode
#   SUIT_MAP = {
#     :S
#   }

  def initialize(value, suit)
    @value = value
    @suit = suit
  end
  
  def render
    "#{VALUE_MAP[value]}#{suit.to_s.downcase}"
  end
end