require 'rspec'
require 'card.rb'
require 'deck.rb'

describe Card do
  subject(:card) { Card.new(5, :S) }
  
  describe "#value" do
    its(:value) { should eq(5) }
    its(:suit) { should eq(:S) }
  end
  
  describe "#render" do
    it "renders display" do
      card.render.should eq("5s")
    end
  end
end

descri